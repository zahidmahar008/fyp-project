import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Checkout from './components/Checkout';
import Header from './components/Header';
import Home from './components/Home';
import ContactUs from './ContactUs';
import Footer from './Footer';
import Login from './Login';
import ShopOnline from './ShopOnline';

function App() {
  return (
    <Router>
      <div className='app'>
        <Switch>
          <Route path='/login'>
            <Login/>
          </Route>  
        <Route path='/checkout'>
          <Header/>
          <Checkout/>
        </Route>  
        <Route path='/contactus'>
          <Header/>
          <ContactUs/>
        </Route> 
        <Route path='/shoponline'>
          <Header/>
          <ShopOnline/>
        </Route>   
          <Route path='/'>
            <Header/>
            <Home/>
            <Footer/> 
          </Route> 
          
        </Switch>
        
      </div>  
    </Router>
  );
}

export default App;
